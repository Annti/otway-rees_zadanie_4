# -*- coding: utf-8 -*-
"""
Aneta Ochedowska
"""

import sys
import socket
import getopt
from Utils import variable_random, encrypt, sanitize, ruwnasie, decrypt


class Server(object):
    """ Main Server class """

    def __init__(self, server_login, key,
                 trusted_server_host, trusted_server_port):

        self.server_login = server_login
        self.key = key
        self.trusted_server_host = trusted_server_host
        self.trusted_server_port = trusted_server_port
        self.server_variable_random_n_b = variable_random()
        self.server = None

    def bind(self):
        """ bind method """

        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((SERVER_ADDRESS, SERVER_PORT))
        self.server.listen(5)

    def run(self):
        """ run method """

        client = self.server.accept()[0]

        client_request_1 = client.recv(2048)
        client_request_2 = client.recv(2048)

        trusted_server = socket.socket()
        trusted_server.connect((self.trusted_server_host, self.trusted_server_port))

        if sanitize(client_request_1, client_request_2):
            trusted_server.send(ruwnasie('ERROR'))
            trusted_server.send(ruwnasie('ERROR'))
            trusted_server.send(ruwnasie('ERROR'))
            client.send(ruwnasie('ERROR'))
            client.send(ruwnasie('ERROR'))
            raise Exception()

        request = str(client_request_1).split(";")

        encrypted_message = encrypt(
            '%s;%s;%s;%s' % (self.server_variable_random_n_b,
                             request[0],
                             request[1],
                             self.server_login),
            self.key)

        trusted_server.send(client_request_1)
        trusted_server.send(client_request_2)
        trusted_server.send(encrypted_message)

        trusted_server_request_1 = trusted_server.recv(2048)
        trusted_server_request_2 = trusted_server.recv(2048)
        trusted_server_request_3 = trusted_server.recv(2048)

        if sanitize(trusted_server_request_1, trusted_server_request_2, trusted_server_request_3):
            client.send(ruwnasie('ERROR'))
            client.send(ruwnasie('ERROR'))
            raise Exception()

        if long(request[0]) != long(trusted_server_request_1):
            raise Exception()

        decrypt_1 = decrypt(trusted_server_request_3, self.key)
        n_b = decrypt_1.split(';')[0]

        if long(n_b) != long(self.server_variable_random_n_b):
            raise Exception()

        client.send(trusted_server_request_1)
        client.send(trusted_server_request_2)

        client.close()


if __name__ == '__main__':

    SERVER_ADDRESS = "localhost"
    SERVER_PORT = None
    SERVER_LOGIN = None
    KEY = None
    TRUSTED_SERVER_HOST = None
    TRUSTED_SERVER_PORT = None

    try:
        OPTS, ARGS = getopt.getopt(sys.argv[1:], "p:l:k:g:q:")

        for opt, arg in OPTS:
            if opt == '-p':
                SERVER_PORT = int(arg)
            if opt == '-l':
                SERVER_LOGIN = arg
            if opt == '-k':
                KEY = int(arg)
            if opt == '-g':
                TRUSTED_SERVER_HOST = arg
            if opt == '-q':
                TRUSTED_SERVER_PORT = int(arg)

        SERVER = Server(server_login=SERVER_LOGIN,
                        key=KEY,
                        trusted_server_host=TRUSTED_SERVER_HOST,
                        trusted_server_port=TRUSTED_SERVER_PORT)
        SERVER.bind()
        SERVER.run()

    except Exception:
        sys.exit(2)
