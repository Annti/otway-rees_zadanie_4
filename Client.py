# -*- coding: utf-8 -*-

"""
Aneta Ochedowska
"""

import socket
import getopt
import sys
from Utils import variable_random, encrypt, sanitize, decrypt, ruwnasie


class Client(object):
    def __init__(self, client_login, key, server_login):
        self.client_login = client_login
        self.key = key
        self.server_login = server_login
        self.client_variable_random_m = variable_random()
        self.client_variable_random_n_a = variable_random()
        self.server = None

    def handle_exc(self):
        self.server.send(ruwnasie('ERROR'))
        self.server.send(ruwnasie('ERROR'))
        sys.exit(2)

    def run(self):
        self.server = socket.socket()
        self.server.connect((SERVER_HOST, SERVER_PORT))

        unencrypted_message = '%s;%s;%s' % (self.client_variable_random_m,
                                            self.client_login, self.server_login)

        to_encrypted_message = '%s;%s;%s;%s' % (self.client_variable_random_n_a,
                                                self.client_variable_random_m,
                                                self.client_login,
                                                self.server_login)

        try:
            encrypted_message = encrypt(to_encrypted_message, self.key)
        except Exception:
            self.handle_exc()

        self.server.send(unencrypted_message)
        self.server.send(encrypted_message)

        server_request1 = self.server.recv(2048)
        server_request2 = self.server.recv(2048)

        if sanitize(server_request1, server_request2):
            raise Exception()

        if long(self.client_variable_random_m) != long(server_request1):
            raise Exception()

        decrypt_s_r_1 = decrypt(server_request2, self.key)
        n_a = decrypt_s_r_1.split(';')[0]

        if long(n_a) != long(self.client_variable_random_n_a):
            raise Exception()

        print 'OK'


if __name__ == '__main__':
    SERVER_HOST = None
    SERVER_PORT = None
    SECURITY = None
    CLIENT_LOGIN = None
    KEY = None
    SERVER_LOGIN = None

    try:
        OPTS, ARGS = getopt.getopt(sys.argv[1:], "h:p:l:k:s:")

        for opt, arg in OPTS:
            if opt == '-h':
                SERVER_HOST = str(arg)
            if opt == '-p':
                SERVER_PORT = int(arg)
            if opt == '-l':
                CLIENT_LOGIN = str(arg)
            if opt == '-k':
                KEY = arg
            if opt == '-s':
                SERVER_LOGIN = str(arg)

        CLIENT = Client(CLIENT_LOGIN, KEY, SERVER_LOGIN)
        CLIENT.run()

    except Exception:
        print 'ERROR'
        sys.exit(2)
