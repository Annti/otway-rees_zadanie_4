Feature: Zdanie z BSK

  Scenario: Proste polaczenie z klientem
    Given Wlaczony trusted serwer z nastepujacymi ustawieniami
      | port | database     |
      | 987  | database.txt |

    Given Wlaczony serwer z nastepujacymi ustawieniami
      | port | login      | key              | trusted_server_host | trusted_server_port |
      | 456  | serveruser | 1234567891234567 | localhost           | 987                 |

    When Klient laczy sie z serwerem uzywajac danych
      | host      | port | login     | key              | server_login |
      | localhost | 456  | anetauser | 1234567891234567 | serveruser   |

    Then Wypisz logi na ekran
