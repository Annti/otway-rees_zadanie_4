from ProcessHandler import ProcessHandler
from behave import *

use_step_matcher("re")


@given("Wlaczony serwer z nastepujacymi ustawieniami")
def step_impl(context):
    conf = context.table[0]

    command = [
        "Server.py",
        "-p", conf['port'],
        "-l", conf['login'],
        "-k", conf['key'],
        "-g", conf['trusted_server_host'],
        "-q", conf['trusted_server_port'],
    ]

    context.server = ProcessHandler(command,
                                    log_file="logs/server_log.txt",
                                    name="Server")


@step("Wlaczony trusted serwer z nastepujacymi ustawieniami")
def step_impl(context):
    conf = context.table[0]

    command = [
        "TrustedServer.py",
        "-p", conf['port'],
        "-d", conf['database'],
    ]

    context.tserver = ProcessHandler(command,
                                     log_file="logs/trusted_server_log.txt",
                                     name="Trusted Server")


@when("Klient laczy sie z serwerem uzywajac danych")
def step_impl(context):
    conf = context.table[0]

    command = [
        "Client.py",
        "-h", conf['host'],
        "-p", conf['port'],
        "-l", conf['login'],
        "-k", conf['key'],
        "-s", conf['server_login'],
    ]
    context.client = ProcessHandler(command,
                                    log_file="logs/client_log.txt",
                                    name="Client")
    context.client.join()


@then("Wypisz logi na ekran")
def step_impl(context):
    context.server.log()
    context.tserver.log()
    context.client.log()
