# -*- coding: utf-8 -*-

"""
Aneta Ochedowska
"""

import getopt
import socket

import sys

from Utils import decrypt, variable_random, encrypt, ruwnasie, sanitize


class TrustedServer(object):
    def __init__(self, trusted_server_address, trusted_server_port, database_dict):
        self.trusted_server_address = trusted_server_address
        self.trusted_server_port = trusted_server_port
        self.database_dict = database_dict
        self.error = False
        self.trusted_server = None

    def bind(self):
        self.trusted_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.trusted_server.bind((self.trusted_server_address, self.trusted_server_port))
        self.trusted_server.listen(5)

    def handle_err(self, server):
        if self.error:
            server.send(ruwnasie('ERROR'))
            server.send(ruwnasie('ERROR'))
            server.send(ruwnasie('ERROR'))
            raise Exception()

    def run(self):

        server = self.trusted_server.accept()[0]
        try:
            server_request_1 = server.recv(2048)
            server_request_2 = server.recv(2048)
            server_request_3 = server.recv(2048)

            if sanitize(server_request_1, server_request_2, server_request_3):
                raise Exception()

            all_m, client_log, server_log = str(server_request_1).split(";")

            descrypt_s_r_1 = decrypt(server_request_2, self.database_dict[client_log])
            descrypt_s_r_2 = decrypt(server_request_3, self.database_dict[server_log])

            if sanitize(descrypt_s_r_1, descrypt_s_r_2):
                raise Exception()

            client_decode = str(descrypt_s_r_1).split(";")
            server_decode = str(descrypt_s_r_2).split(";")
            if long(client_decode[1]) != long(server_decode[1]) != long(all_m):
                raise Exception()

            if client_decode[2] != client_log != server_decode[2]:
                raise Exception()

            if client_decode[3] != server_log != server_decode[3]:
                raise Exception()

            secret_key = variable_random()

            message_for_client = encrypt(' % s; % s' % (client_decode[0], secret_key),
                                         self.database_dict[client_log])

            message_for_server = encrypt(' % s; % s' % (server_decode[0], secret_key),
                                         self.database_dict[server_log])
            server.send('%s' % all_m)
            server.send(message_for_client)
            server.send(message_for_server)

        except KeyError:
            pass
        except Exception:
            self.handle_err(server)


if __name__ == '__main__':

    TRUSTED_SERVER_ADDRESS = "localhost"
    TRUSTED_SERVER_PORT = None
    DATABASE = None

    try:
        OPTS, ARGS = getopt.getopt(sys.argv[1:], "p:d:")
    except getopt.GetoptError:
        sys.exit(2)

    for opt, arg in OPTS:
        if opt == '-p':
            TRUSTED_SERVER_PORT = int(arg)
        if opt == '-d':
            DATABASE = arg

    DB = {}
    try:
        with open(DATABASE, 'r') as database:
            for line in database:
                k, v = line.strip().split(';')
                DB[k] = v

        TRUSTED_SERVER = TrustedServer(trusted_server_address=TRUSTED_SERVER_ADDRESS,
                                       trusted_server_port=TRUSTED_SERVER_PORT,
                                       database_dict=DB)
        TRUSTED_SERVER.bind()
        TRUSTED_SERVER.run()

    except Exception:
        sys.exit(2)
