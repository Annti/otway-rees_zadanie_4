# -*- coding: utf-8 -*-

"""
Aneta Ochedowska
"""

import random
import re
from Crypto.Cipher import AES


def variable_random():
    return random.randint(0, 1000000000000)


def sanitize(*msg):
    for m_s in msg:
        if re.match("ERROR", m_s):
            return True


def ruwnasie(string):
    length_message = len(string)
    add_ch = 2048 - length_message
    return string + '=' * add_ch


def encrypt(message, key):
    try:
        obj = AES.new(str(key), AES.MODE_CBC, 'Must be 16charts')
        to_enc_message = ruwnasie(message)
        enc_message = obj.encrypt(to_enc_message)
        return enc_message
    except Exception:
        raise Exception()


def decrypt(message, key):
    try:
        obj2 = AES.new(str(key), AES.MODE_CBC, 'Must be 16charts')
        decoded = obj2.decrypt(message)
        decoded = re.sub('=*$', '', decoded)
        return decoded
    except Exception:
        raise Exception()
